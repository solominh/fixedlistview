package com.example.hoangminh.fixedlistview;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Window;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    private RecyclerView mRecyclerView;
    private MyAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    List<String> myDataset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        myDataset = new ArrayList<>();
        myDataset.add("" + "asd\nsda");
        for (int i = 0; i < 30; i++) {
            myDataset.add("" + i);
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                Point dimen = new Point();
                dimen.x = mRecyclerView.getWidth();
                //dimen.y = mRecyclerView.getHeight();
                dimen.y = 2000;

                mAdapter = new MyAdapter(myDataset, dimen);
                mRecyclerView.setAdapter(mAdapter);
            }
        });


    }


}
