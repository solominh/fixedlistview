package com.example.hoangminh.fixedlistview;

import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hoangminh on 9/23/15.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private List<String> mDataset;
    private List<Integer> heightSets;
    int count = 0;
    float ratio = 1;
    boolean isFirstTime = true;
    Point dimen;
    boolean canBigger = false;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView;

        public ViewHolder(View v) {
            super(v);
            mTextView = (TextView) v.findViewById(R.id.tv_name);
        }
    }

    public MyAdapter(List<String> myDataset, Point dimen) {
        mDataset = myDataset;
        this.dimen = dimen;
        heightSets = new ArrayList<>();
    }

    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mTextView.setText(mDataset.get(position));
        if (canBigger) {
            if (position == mDataset.size() - 1) {

                int sum = 0;
                for (int i = 0; i < heightSets.size() - 1; i++)
                    sum += heightSets.get(i) * ratio;

                holder.mTextView.setLayoutParams(new RecyclerView.LayoutParams(dimen.x, dimen.y - sum));
            } else {
                holder.mTextView.setLayoutParams(new RecyclerView.LayoutParams(dimen.x, (int) (ratio * heightSets.get(position))));
            }

        }

        if (!isFirstTime)
            return;

        holder.mTextView.post(new Runnable() {
            @Override
            public void run() {
                /*if (heightSets == null || heightSets.size() <= position || heightSets.get(position) == null)
                    return;*/

                int textHeight = holder.mTextView.getHeight();
                count += textHeight;
                heightSets.add(position, textHeight);

                if (count >= dimen.y) {
                    isFirstTime = false;
                    return;
                }

                if (heightSets.size() == mDataset.size()) {
                    ratio = dimen.y / (float) count;
                    isFirstTime = false;
                    canBigger = true;
                    notifyDataSetChanged();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
